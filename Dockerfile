FROM maven:3.6.3-jdk-14
COPY /target/spring-petclinic-2.3.0.BUILD-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch spring-petclinic-2.3.0.BUILD-SNAPSHOT.jar'
ENTRYPOINT ["java","-jar","spring-petclinic-2.3.0.BUILD-SNAPSHOT.jar"]
