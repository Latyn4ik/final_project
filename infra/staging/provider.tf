provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    region = "eu-west-1"
    bucket = "stagingterraform"
    key = "terraform_backup_staging.tfstate"
  }
}