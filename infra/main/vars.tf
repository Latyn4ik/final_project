variable RDS_PASSWORD {}
variable RDS_USERNAME {}

variable "DOCKER_EC2_AMI" {
  default = "ami-07d7061189d56c1eb"
}

variable "EC2_TYPE" {
  default = "t2.micro"
}

variable "SSH_KEY_PATH_APP_INSTANCE" {
  default = "aws_key.pem"
}
variable "SSH_KEY_PATH_ZABBIX_INSTANCE" {
  default = "centos_key.pem"
}

variable "APP_INSTANCE_USER" {
  default = "ec2-user"
}